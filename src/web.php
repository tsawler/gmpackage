<?php

Route::group(['middleware' => ['web']], function () {
    Route::post('/gm/joinlist', '\Tsawler\GmPackage\MailingListController@postJoinList');
    Route::post('/gm/contact-gair', '\Tsawler\GmPackage\MailingListController@postContactGair');
});
