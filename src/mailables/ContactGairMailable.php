<?php

namespace Tsawler\GmPackage;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactGairMailable extends Mailable
{

    use Queueable, SerializesModels;

    public $sender_name;
    public $sender_website;
    public $sender_email;
    public $coaching;
    public $speaking;
    public $what_they_said;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->sender_name = $data['sender_name'];
        $this->sender_website = $data['sender_website'];
        $this->sender_email = $data['sender_email'];
        $this->coaching = $data['coaching'];
        $this->speaking = $data['speaking'];
        $this->what_they_said = $data['what_they_said'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Contact from website')->markdown('gmpackage::contact-gair');
    }
}
