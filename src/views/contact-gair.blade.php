@component('mail::message')
# Message from Website

The following message was recieved from the website:

Name: {{ $sender_name }}

Email: {{ $sender_email }}

Website: {{ $sender_website }}

Coaching: {{ $coaching }}

Speaking: {{ $speaking }}

Message:

{{ $what_they_said }}
@endcomponent
