<?php namespace Tsawler\GmPackage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Spatie\Newsletter\NewsletterFacade;

/**
 * Class MailChimpController
 * @package App\Http\Controllers
 */
class MailingListController extends Controller
{


    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postJoinList()
    {

        $email = Input::get('EMAIL');
        $fname = Input::get('FNAME');
        $lname = Input::get('LNAME');
        $message = "";
        $passed = false;

        if (($email == '') || ($fname == '') || ($lname == '')) {
            return back()
                ->with('error', 'You must include your first name, last name, and an email address!');
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            Session::put('modal_alert', 'Please enter a valid email address!');

            return back();
        }


        if (NewsletterFacade::isSubscribed($email)) {
            $message = "You are already subscribed to our mailing list!";
        } else {
            $passed = true;
            NewsletterFacade::subscribe($email, ['FNAME' => $fname, 'LNAME' => $lname]);
        }

        if ($passed) {
            return back()->with('modal_alert', "Thanks! You have been added to our list!");
        } else {
            return back()->with('modal_alert', $message);
        }
    }


    public function postContactGair(Request $request)
    {
        $this->validate($request, [
            'name'           => 'required',
            'email'                => 'email',
            'g-recaptcha-response' => 'required|captcha',
        ]);

        $speaking = "No";
        $coaching = "No";

        $name = Input::get('name');
        $email = Input::get('email');
        $website = Input::get('website');
        if(Input::has('coaching'))
            $coaching = "Yes";
        if (Input::has('speaking'))
            $speaking = "Yes";
        $what_they_said = Input::get('message');

        $data = [
            'sender_name'    => $name,
            'sender_website' => $website,
            'sender_email'   => $email,
            'coaching' => $coaching,
            'speaking' => $speaking,
            'what_they_said' => $what_they_said,
        ];

        Mail::to('info@gairmaxwell.com')
            ->queue(new ContactGairMailable($data));


        return back()->with('modal_alert', "Thanks! Your message has been sent!");
    }
}
