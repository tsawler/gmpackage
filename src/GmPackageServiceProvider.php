<?php namespace Tsawler\GmPackage;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;

/**
 * Class WheelsPackageServiceProvider
 * @package Tsawler\WheelsPackage;
 */
class WheelsPackageServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *c
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/web.php');
        $this->loadViewsFrom(__DIR__ . '/views', 'gmpackage');
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }

}
